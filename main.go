package main

import (
	"log"
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/controllercubiomes/statistics/db"
)

func main() {
	dbCred := db.DBCred{
		DB_HOST:     os.Getenv("POSTGRES_HOST"),
		DB_USER:     os.Getenv("POSTGRES_USER"),
		DB_NAME:     os.Getenv("POSTGRES_DB"),
		DB_PASSWORD: os.Getenv("POSTGRES_PASSWORD"),
	}
	conn, err := dbCred.ConnectToDB()
	if err != nil {
		log.Fatal(err)
	}
	dbc := db.DB{Conn: conn}
	r := gin.Default()
	{
		r.GET("statistics/api/uniquekeysoverperiod", func(c *gin.Context) {
			total, err := dbc.UniqueKeysOverPeriod()
			if err != nil {
				log.Println(err)
			}
			c.JSON(200, gin.H{"unique_keys_over_period": total})
		})
		r.GET("statistics/api/verifiedseedslasthour", func(c *gin.Context) {
			total, err := dbc.VerifiedSeedsLastHour()
			if err != nil {
				log.Println(err)
			}
			c.JSON(200, gin.H{"verified_seeds_lasthour": total})
		})
		r.GET("statistics/api/sps", func(c *gin.Context) {
			sps, err := dbc.GetSPS()
			if err != nil {
				log.Println(err)
			}
			c.JSON(200, gin.H{"seeds_per_secound": sps})
		})
		r.GET("statistics/api/totalscanned", func(c *gin.Context) {
			total, err := dbc.GetTotalScanned()
			if err != nil {
				log.Println(err)
			}
			c.JSON(200, gin.H{"total_scanned_seeds": total})
		})
		r.GET("statistics/api/totalviable", func(c *gin.Context) {
			total, err := dbc.GetTotalViable()
			if err != nil {
				log.Println(err)
			}
			c.JSON(200, gin.H{"total_viable_seeds": total})
		})
		r.GET("statistics/api/unverifiedchunks", func(c *gin.Context) {
			total, err := dbc.GetUnverifiedchunks()
			if err != nil {
				log.Println(err)
			}
			c.JSON(200, gin.H{"unverifiedchunks": total})
		})

	}
	if err := r.Run(":3030"); err != nil {
		log.Println(err)
	}
}
