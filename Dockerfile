FROM alpine:latest
RUN apk update && apk add ca-certificates shadow libcap && rm -rf /var/cache/apk/*
RUN groupadd -g 1000 server
RUN useradd -u 1000 -g 1000 -d / server
COPY ./statistics /server
RUN setcap 'cap_net_bind_service=+ep' /server # Allow server to bind privileged ports

EXPOSE 3030
USER server
ENTRYPOINT ["/server"]
