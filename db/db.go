package db

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	_ "github.com/lib/pq"
)

type DBCred struct {
	DB_HOST     string
	DB_USER     string
	DB_PASSWORD string
	DB_NAME     string
}

type DB struct {
	Conn *sql.DB
}

func (dbc DBCred) ConnectToDB() (*sql.DB, error) {
	dbinfo := ""
	if dbc.DB_PASSWORD == "" {
		dbinfo = fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable",
			dbc.DB_HOST, dbc.DB_USER, dbc.DB_NAME)
	} else {
		dbinfo = fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=disable",
			dbc.DB_HOST, dbc.DB_USER, dbc.DB_PASSWORD, dbc.DB_NAME)
	}

	db, err := sql.Open("postgres", dbinfo)
	if err != nil {
		return nil, err
	}

	return db, db.Ping()
}

func (db DB) UniqueKeysOverPeriod() (unique int, err error) {
	err = db.Conn.QueryRow("SELECT count(DISTINCT fo.key) FROM (SELECT key FROM chunks WHERE startTime > '$1' UNION ALL SELECT verifier as key FROM chunks WHERE verStartTime > '$1') AS fo;", time.Now().Add(-time.Hour)).Scan(&unique)
	if err != nil {
		log.Println(err)
	}
	return
}

func (db DB) VerifiedSeedsLastHour() (unique int, err error) {
	err = db.Conn.QueryRow("SELECT max(seedsFound) FROM chunks WHERE verified = TRUE AND found_time > '$1';", time.Now().Add(-time.Hour)).Scan(&unique)
	if err != nil {
		log.Println(err)
	}
	return
}

func (db DB) GetSPS() (count int, err error) {
	row := db.Conn.QueryRow("SELECT count(*) FROM chunks WHERE (startTime > now() - interval '13 hours' AND seedsfound != null) OR (verStartTime > now() - interval '13 hours' AND verified != false);")
	if err != nil {
		return
	}
	err = row.Scan(&count)
	if err != nil {
		log.Println(err)
		return
	}
	count = count * (1 << 30) / 13 / 60 / 60
	return
}

func (db DB) GetTotalScanned() (count int, err error) {
	row := db.Conn.QueryRow("SELECT count(*) FROM chunks WHERE verified = TRUE;")
	if err != nil {
		return
	}
	err = row.Scan(&count)
	if err != nil {
		log.Println(err)
		return
	}
	count = count * (1 << 30)
	return
}

func (db DB) GetTotalViable() (count int, err error) {
	row := db.Conn.QueryRow("SELECT count(*) FROM seeds WHERE verified = TRUE;")
	if err != nil {
		return
	}
	err = row.Scan(&count)
	if err != nil {
		log.Println(err)
	}
	return
}

func (db DB) GetUnverifiedchunks() (count int, err error) {
	err = db.Conn.QueryRow("SELECT count(*) FROM chunks WHERE verified = FALSE;").Scan(&count)
	if err != nil {
		log.Println(err)
	}
	return
}
