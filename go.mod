module gitlab.com/controllercubiomes/statistics

go 1.14

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/lib/pq v1.5.2
	gitlab.com/controllercubiomes/server v0.0.3
	gitlab.com/controllercubiomes/util v0.1.4
)
